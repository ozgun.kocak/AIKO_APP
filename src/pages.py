from flask import (
    Blueprint,
    flash,
    render_template,
    redirect,
    request,
    url_for
)
from datetime import datetime
from src.database import get_db
from flask_login import login_required, current_user

bp = Blueprint("pages", __name__)


##Home Page
@bp.route("/home")
@login_required
def home():
    db = get_db()
    cursor = db.cursor(dictionary=True)
    cursor.execute("SELECT name, surname FROM USERS WHERE mail = %s", (current_user.id,))
    result = cursor.fetchone()
    return render_template("pages/home.html", result=result)

## Daily Scrum Form Page
@bp.route("/form", methods=("GET", "POST"))
@login_required
def form():
    # Getting the project/product names per user from the database dynamically. 
    #We're gonna use this info to print the current info in the form.
    mail = current_user.id
    db = get_db()
    cursor = db.cursor(dictionary=True)
    cursor.execute("SELECT PROJECT FROM ALLOCATIONS WHERE EMPLOYEE = %s", (mail,))  # Getting the column names from the table
    result = cursor.fetchall()
    p_names = []
    for row in result:
        p_names.append(row["PROJECT"])

    # Handling the form data
    if request.method == "POST":

        #Date format conversion -> final result : YYYY-MM-DD 
        unformatted_date = request.form["date"]
        date_obj = datetime.strptime(unformatted_date, "%Y-%m-%d")
        date = date_obj.strftime("%Y-%m-%d")
        time_period = date_obj.strftime("%Y-%m")       #To be indicated within ALLOCATIONS table
        
        # Checking if the user has entered date unexpected date ( date indicating a future day )
        if(date_obj > datetime.now()):
            flash("You can't log hours for future days", category="error")
            return redirect(url_for("pages.form"))
        
        # Checking if the user has already entered the data for the day
        cursor.execute("SELECT * FROM TIME_TRACKING WHERE mail = %s AND date = %s", (mail, date,))
        entry = cursor.fetchone()
        if not entry:
            # Preparing the query schema elements
            columns = ", ".join(p_names)
            placeholders = ", ".join(["%s"] * len(p_names))

            worked_projects = ""
            worked_hours = []
            tot_worked_hours = 0.0


            for p_name, hours in request.form.items():
                if p_name != "date":
                    if hours != 0.0:
                        worked_projects += p_name + ", "
                        worked_hours.append(hours)
                        tot_worked_hours += float(hours)

                        # Checking if the user has worked more than 8 hours
                        if( tot_worked_hours > 8.0):    
                            flash("You can't work more than 8 hours a day.", category="error")
                            cursor.close()
                            return redirect(url_for("pages.form"))

            # Checking if the user has entered the hours for the projects/products
            if(tot_worked_hours == 0):
                flash("Please indicate the hours per project/product you want to submit", category="error")
                cursor.close()
                return redirect(url_for("pages.form"))
                    
            # Implementing the successful finish flashes
            if( tot_worked_hours < 8.0):
                message = "Missing logged hours detected within the daily form"
                category = "info"
                query = f"INSERT INTO NOTIFICATIONS (mail, text, date, type) VALUES (%s, %s, %s, %s)"
                flash(message, category=category)
                cursor.execute(query, (current_user.id, message, date, category))
            else:
                flash("You have successfully logged your hours", category="success")
                        
            # Inserting the data to the database
            query = f"INSERT INTO TIME_TRACKING (mail, date, {columns}) VALUES (%s, %s, {placeholders})"
            cursor.execute(query, (mail, date, *worked_hours),)
            db.commit()
            # Updating the worked hours in the ALLOCATIONS table
            for p, h in zip(worked_projects.split(", ")[:-1], worked_hours):
                query = """
                    UPDATE ALLOCATIONS 
                    SET HOURS_WORKED = HOURS_WORKED + %s 
                    WHERE EMPLOYEE = %s AND PROJECT = %s AND TIME_PERIOD = %s
                    """
                cursor.execute(query, (float(h), mail, p, time_period))
                db.commit()

            cursor.close()
        
        # In case the user wants to update the logged hours for the day (Shall log every project/product again)
        else:
            # Checking if the user has worked more than 8 hours
            tot_worked_hours = 0.0
            for p_name, hours in request.form.items():
                if p_name != "date":
                    if hours != 0.0:
                        tot_worked_hours += float(hours)
                        if( tot_worked_hours > 8.0):  
                            flash("You can't log more than 8 hours for a day.", category="error")
                            return redirect(url_for("pages.form"))
            
            # Checking if the user has entered the hours for the projects/products
            if(tot_worked_hours == 0):
                flash("Please indicate the hours per project/product you want to submit", category="error")
                cursor.close()
                return redirect(url_for("pages.form"))
            
            
            # Updating the data in the database
            for p_name, hours in request.form.items():
                if p_name != "date":
                    if(entry[p_name] != hours):
                        #First get the previous hours logged for the project/product
                        cursor.execute(f"SELECT {p_name} FROM TIME_TRACKING WHERE mail = %s AND date = %s", (mail, date,))
                        previous_log = cursor.fetchone()[p_name]
                        #Then update the hours in the TIME_TRACKING table
                        query = f"UPDATE TIME_TRACKING SET {p_name} = {hours} WHERE mail = %s AND date = %s"
                        cursor.execute(query, (mail, date,))
                        #Then update the hours in the ALLOCATIONS table
                        query = """
                            UPDATE ALLOCATIONS
                            SET HOURS_WORKED = HOURS_WORKED - %s + %s
                            WHERE EMPLOYEE = %s AND PROJECT = %s AND TIME_PERIOD = %s
                                """
                        cursor.execute(query, (float(previous_log), float(hours), mail, p_name, time_period))
                        db.commit()
            
            # Implementing the successful finish flashes
            if( tot_worked_hours < 8.0):
                message = "Missing logged hours detected within the daily form"
                category = "info"
                query = f"INSERT INTO NOTIFICATIONS (mail, text, date, type) VALUES (%s, %s, %s, %s)"
                flash(message, category=category)
                cursor.execute(query, (current_user.id, message, date, category))
            else:
                flash("You have successfully logged your hours", category="success")

        # User shall redirect to the personal_tsheet page after every successful form submission
        return redirect(url_for("pages.personal_tsheet"))          
    return render_template("pages/form.html", p_names=p_names)


@bp.route("/notif_center")
@login_required
def notif_center():
    db = get_db()
    cursor = db.cursor(dictionary=True)
    cursor.execute("SELECT text, date, type FROM NOTIFICATIONS WHERE mail = %s", (current_user.id,))
    notifs = cursor.fetchall()
    cursor.close()

    return render_template("pages/notif_center.html", notifs = notifs)


@bp.route("/personal_tsheet")
@login_required
def personal_tsheet():
    #Date format conversion -> final result : YYYY-MM-DD 
    date = datetime.now()
    time_period = date.strftime("%Y-%m")       #To be indicated within ALLOCATIONS table

    db = get_db()
    cursor = db.cursor(dictionary=True)
    cursor.execute("SELECT PROJECT, ASSIGNED_HOURS, HOURS_WORKED FROM ALLOCATIONS WHERE EMPLOYEE = %s and TIME_PERIOD = %s", (current_user.id, time_period,))
    allocations = cursor.fetchall()
    return render_template("pages/personal_tsheet.html", allocations = allocations)
