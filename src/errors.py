from flask import Flask,render_template, current_app, request

def page_not_defined(e):
    current_app.logger.info(f"{e.name} error ({e.code}) at {request.url}")
    return render_template("errors/404.html"), 404
