import mysql.connector
from flask import current_app, g

# Initiates the database connection and adjusts the closing function
def init_app(app):
    app.teardown_appcontext(close_db)


def close_db(exc=None):
    db = g.pop("db", None)

    if db is not None:
         db.close()

# Connecting to the database
def get_db():
    if "db" not in g:
        g.db = mysql.connector.connect(
            host=current_app.config["DATABASE_HOST"],
            user=current_app.config["DATABASE_USER"],
            password=current_app.config["DATABASE_PW"],
            database=current_app.config["DATABASE"],
            autocommit=True
        )
    return g.db