"""
*   Author: Özgün Koçak
*   Project Starting Date 01.01.2024
"""

from flask import Flask
import os
from dotenv import load_dotenv
import mysql.connector
from flask_login import LoginManager, UserMixin

from src import database, errors, pages, login

load_dotenv() # Loads environment variables
login_manager = LoginManager()  # Initiates the login manager

class User(UserMixin):
    def __init__(self, id):
        self.id = id

  
@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

def create_app():
    app = Flask(__name__)  # Initiates the Flask App
    app.config.from_prefixed_env()   # Loads the env variables with FLASK_ prefix
    app.logger.setLevel("INFO")
    login_manager.init_app(app) # Initiates the login manager
    login_manager.login_view = "login.login"  # Sets the login view for the login manager
    login_manager.login_message_category = "error"  # Sets the login message for the login manager


    # Initiates the database connection with caution
    try:
        database.init_app(app) 
    except mysql.connector.Error as err:
        print(f"Error connecting to MySQL: {err}")
        raise err

    # Registers the page blueprints to the app
    app.register_blueprint(login.bp)
    app.register_blueprint(pages.bp)
    app.register_error_handler(404, errors.page_not_defined)

    # Prints the environment info to the terminal within debug mod
    app.logger.debug(f"Current Environment: {os.getenv('ENVIRONMENT')}")
    app.logger.debug(f"Using Database: {os.getenv('DATABASE')}")

    return app