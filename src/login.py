
from flask import (
    Blueprint,
    render_template,
    redirect,
    request,
    url_for,
    flash
)
from flask_login import logout_user, login_user
from src.__init__ import User
from src.database import get_db

bp = Blueprint("login", __name__)


@bp.route("/login", methods=("GET", "POST"))
def login():
    db = get_db()
    cursor = db.cursor(dictionary=True)
    cursor.execute("SELECT mail, password FROM USERS")
    result = cursor.fetchall()

    mails = [row["mail"] for row in result]

    if request.method == "POST":
        mail = request.form["mail"]
        pw = request.form["password"]

        user_data = next((row for row in result if row["mail"] == mail), None)
        
        if user_data and user_data["password"] != pw:
            flash("Mail and password doesn't match.", category="error")
        elif user_data and user_data["password"] == pw:
            user = User(mail)
            login_user(user)
            return redirect(url_for("pages.home"))

    return render_template("login.html", mails=mails)

@bp.route("/logout")
def logout():
    logout_user()
    flash("You have been logged out.", category="info")
    return redirect(url_for("login.login"))
