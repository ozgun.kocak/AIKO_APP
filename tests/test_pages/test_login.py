from src.database import get_db
from flask import request
from src.login import login
def test_blueprint_registration(app):
    assert "login","home" in app.blueprints

def test_db_get_mail_password_list(app, expected_result):
    with app.app_context():
        db = get_db()
        cursor = db.cursor(dictionary=True)
        cursor.execute("SELECT mail, password FROM USERS WHERE name='test_name'")
        result = cursor.fetchall()

        assert len(result) > 0
        mails = [row["mail"] for row in result]

        for i, row in enumerate(result):
            assert expected_result[i] == row # Checks if the mail and pw match
            assert expected_result[i]["mail"] == mails[i] #Checks the mail list

def test_form_submission(app):
    with app.test_client() as client:
        response = client.post("/login", data={"mail":"test_mail", "password":"test_pw"})
        assert request.form["mail"] == "test_mail"
        assert request.form["password"] == "test_pw"

