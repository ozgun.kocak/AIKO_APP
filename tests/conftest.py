from src.__init__ import create_app
import pytest

@pytest.fixture
def app():
    app = create_app()
    return app

@pytest.fixture
def expected_result():
    return [{"mail": "test@aikospace.com", "password": "test_pw"}]


