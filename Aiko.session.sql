CREATE TABLE IF NOT EXISTS ALLOCATIONS (
    EMPLOYEE VARCHAR(255),
    PROJECT VARCHAR(255),
    TIME_PERIOD DATE,
    ASSIGNED_HOURS INT,
    HOURS_WORKED FLOAT,
    PRIMARY KEY (EMPLOYEE, PROJECT, TIME_PERIOD)
);
CREATE TABLE IF NOT EXISTS PROJECTS (
    NAME VARCHAR(255) PRIMARY KEY,
    MANAGER VARCHAR(255),
    PRIORITY INT,
    STATUS VARCHAR(255)
);
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'orbital_OLIVER',
        'kayaltan@hotmail.com ',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'cloudy_CHARLES',
        'kayaltan@hotmail.com ',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'gifted_GENE',
        'kayaltan@hotmail.com ',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES ('ATRIA', 'kayaltan@hotmail.com ', 1, 'TESTING');
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES ('AIX', 'kayaltan@hotmail.com ', 1, 'TESTING');
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'EARTHNEXT',
        'kayaltan@hotmail.com ',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES ('IoT4EO', 'ozgun.slash@gmail.com', 1, 'TESTING');
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'SAFEXPLAIN',
        'ozgun.slash@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'ENLIGHTEN',
        'ozgun.slash@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'IRIDE_FOS',
        'ozgun.slash@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES ('IOSL', 'ozgun.slash@gmail.com', 1, 'TESTING');
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES ('DEEPSAR', 'ozgun.slash@gmail.com', 1, 'TESTING');
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES ('SPRING', 'ozgun.slash@gmail.com', 1, 'TESTING');
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'jolly_JUNO',
        'test@aikospace.com  ',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'jolly_JANE',
        'test@aikospace.com  ',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'MARLA',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'AISAC_RECON',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'AISAC_RL',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'SUPER_RESOLUTION',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'SAT_PLANNER',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'tuning_TAYLOR',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'IT_Support',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'VENV_4_CHARLES_AND_OLIVER',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'BUZZ',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'Software_Department',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'ML_Department',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'Automation_Department',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'VENV_Department',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'HW_Eng_Department',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO PROJECTS (NAME, MANAGER, PRIORITY, STATUS)
VALUES (
        'Proposal_France_2030',
        'vinassa.virginia@gmail.com',
        1,
        'TESTING'
    );
INSERT INTO ALLOCATIONS (
        EMPLOYEE,
        PROJECT,
        TIME_PERIOD,
        ASSIGNED_HOURS,
        HOURS_WORKED
    )
VALUES (
        'ozgun.slash@gmail.com',
        'Software_Department',
        '2024-02',
        30,
        0
    );
INSERT INTO ALLOCATIONS (
        EMPLOYEE,
        PROJECT,
        TIME_PERIOD,
        ASSIGNED_HOURS,
        HOURS_WORKED
    )
VALUES (
        'ozgun.slash@gmail.com',
        'Automation_Department',
        '2024-02',
        30,
        0
    );
INSERT INTO ALLOCATIONS (
        EMPLOYEE,
        PROJECT,
        TIME_PERIOD,
        ASSIGNED_HOURS,
        HOURS_WORKED
    )
VALUES (
        'ozgun.slash@gmail.com',
        'IT_Support',
        '2024-02',
        20,
        0
    );
DELETE FROM TIME_TRACKING
WHERE mail = 'ozgun.slash@gmail.com';
USE AIKO_DATABASE;
CREATE TABLE IF NOT EXISTS NOTIFICATIONS (
    mail VARCHAR(50),
    text VARCHAR(250),
    date DATE,
    type VARCHAR(30)
);